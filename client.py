#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys

# Constantes. Dirección IP del servidor y contenido a enviar
try:
    IP = sys.argv[1]
    PUERTO = int(sys.argv[2])
    REGISTER = str(sys.argv[3])
    CORREO = str(sys.argv[4])
    TIEMPOEXP = str(sys.argv[5])
    DIRECCION = REGISTER.upper() + " sip:" + CORREO + " SIP/2.0" "\r\n" + "Expires:" + str(TIEMPOEXP) + "\r\n"
except IndexError:
    sys.exit("Hay poner:python3 client.py:ip,puerto,register,correo,tiempoexp")

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((IP, PUERTO))
    print("Enviando:", DIRECCION)
    my_socket.send(bytes(DIRECCION, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    print('Recibido -- ', data.decode('utf-8'))

print("Socket terminado.")

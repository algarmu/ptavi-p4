#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import json
from time import time, strftime, gmtime


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    DicUsers = {}

    def json2register(self):
        try:
            with open('registered.json', 'r') as jsonfile:
                self.dict = json.load(jsonfile)
        except FileNotFoundError:
            print("Carpeta creada")

    def register2json(self):
        with open('registered.json', "w") as fiche:
            json.dump(self.DicUsers, fiche, indent=4)

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        if len(self.DicUsers) == 0:
            self.json2register()

        self.wfile.write(b'SIP/2.0 200 OK \r\n')
        line = self.rfile.read()
        formato = line.decode('utf-8')
        IP = self.client_address[0]
        PUERTO = self.client_address[1]
        REGISTER = formato.split(" ")[0].lower()
        CORREO = formato.split(" ")[1].split(":")[1]
        TIEMPOEXP = formato.split(" ")[2].split("\r\n")[1].split(":")[1]
        DIRECCION = ('IP: ' + str(IP) + ' PUERTO: ' + str(PUERTO))
        IP2 = "address: " + IP
        SUMA = int(TIEMPOEXP) + int(time())
        GMT = "%Y-%m-%d %H:%M:%S"
        HORA = strftime(GMT, gmtime(SUMA))
        HORANOW = strftime(GMT, gmtime(time()))

        if REGISTER == 'register':
            self.DicUsers[CORREO] = IP2, HORA

            if int(TIEMPOEXP) == 0:
                print(CORREO + " ha sido eliminado")
                del self.DicUsers[CORREO]

            if int(TIEMPOEXP) < 0:
                sys.exit("Error, no puedes añadir valores negativos")

            for CORREO in self.DicUsers.copy():
                if HORANOW >= self.DicUsers[CORREO][1]:
                    del self.DicUsers[CORREO]

        print("******************" + "\n" + "direcciones: " + "\n" + DIRECCION)
        print(self.DicUsers)
        self.register2json()


if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request
    PUERTO = int(sys.argv[1])
    serv = socketserver.UDPServer(('', PUERTO), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
